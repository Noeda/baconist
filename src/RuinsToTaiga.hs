{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}

module RuinsToTaiga
  ( ruinsToTaiga
  )
where

import           CM.Coords
import           CM.Portal
import           CM.LevelLike
import           CM.TileWorld

import           SnowHub
import           Tiles

ruinsToTaiga
  :: LevelLike (Level w) Coords2D Tile => PortalGroup -> WBuilder w ()
ruinsToTaiga pgroup = do
  snow_hub_group <- makePortalGroup
    [ ('O', OnLeft)
    , ('o', OnRight)
    , ('P', OnLeft)
    , ('p', OnRight)
    , ('Q', OnLeft)
    , ('q', OnRight)
    ]
  putLevelOverrides
    [pgroup, snow_hub_group]
    [ "####KLM#####"
    , "#Tt......Tt#"
    , "#tT.....TtT#"
    , "#tT.t...tTT#"
    , "#TT.T...ttT#"
    , "#tT.....TTT#"
    , "#Tt...TTTtT##################"
    , "#TT.....ttTTTTTTTTTTTTTTTTTt#"
    , "#tT.....TTTtttttTttttTTTTttT#"
    , "#TtT..TTTtTtttTTt.tT.tt.tttt#"
    , "#TTTt............`........TT#"
    , "#tTtT.....`...............tT#"
    , "#TtTttTTTtTttttt.`.T..T...Tt#"
    , "#TtTttTTTtTTttttttttttt.`.Tt#"
    , "#TtTttTTTtTTTTTTTTTTTTTT..tT#"
    , "#TtTttTTTtTTTTTTTTTTTTTT..tT#"
    , "#TtTttTTTtTTTTTTTTTTTTTt..tT#"
    , "#TtTttTTTtTTTTTTTTTTTTt...tT#"
    , "#TtTttTTTtTTTTTTTTTTTTt..`tT#"
    , "#TtTttTTTtTTTTT.TTTTtT....tT#"
    , "#TtTttTTTtTTtTt.t.TtTT....tT##"
    , "#TtTttTTTtT.........`.....tTT#"
    , "#TtTttTT..`...............tTt#"
    , "#TtTttT.....tTTTTTTTTt`..`tTt#"
    , "#TtTtt....ttTTTTTTTTTT~~~~~Tt#"
    , "#TtTttT`..OTTTTTTTTTT~~~~~~TT#"
    , "#TtTt.T...PTTTTTTTTT~~~~~~tTt#"
    , "#TtTttT...QTTTTTTTTTTT~~~~tTT#"
    , "#TtTttTTTtTTTTTTTTTTTTttTttTt#"
    , "#TtTttTTTtTTTTTTTTTTTTtTTTtTT#"
    , "#TtTttTTTtTTTTTTTTTTTTTttttTT#"
    , "##############################"
    ]
    [ ('#', TaigaWall)
    , ('T', TaigaTree)
    , ('t', TaigaTree2)
    , ('.', TaigaFloor)
    , ('`', TaigaBush)
    ]
  snowHub snow_hub_group
