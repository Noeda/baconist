{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}

module SnowHub
  ( snowHub
  )
where

import           CM.Coords
import           CM.Portal
import           CM.LevelLike
import           CM.TileWorld

import           Tiles

snowHub :: LevelLike (Level w) Coords2D Tile => PortalGroup -> WBuilder w ()
snowHub pgroup = do
  putLevelOverrides
    [pgroup]
    [ "###############################################"
    , "##TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT#"
    , "##TTTTTTTTTTTTTTTTTTtTtTTTTTTTTTTTTTTTTTTTTTTT#"
    , "##TTTtTTtTT.........TTTTTTtTTtTTTTTTTTTTTTTTTT#"
    , "##tTT.....................................ttTT#"
    , "##tT...............................=====....tT#"
    , "##TT.........................T...===~~~=....tT#"
    , "##tt.............................==~~~~=T...tT#"
    , "##TtT............................==~~~~=....tT#"
    , "o........................t.......====~==....tT#"
    , "p.................................======t...TT#"
    , "q...........................................TT#"
    , "##t........................................ttT#"
    , "##Tt................................T......tTT#"
    , "##TTT....................................tTtTT#"
    , "##TTTTtTTtTtTTTtTTt................tTtttTTTTTT#"
    , "##TTTTTTTTTTTTTTTTTTTTTTTtTTtT...tTTTTTTTTTTTT#"
    , "##TTTTTTTTTTTTTTTTTTTTTTTTTTTtttTTTTTTTTTTTTTT#"
    , "##TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTtTTTTTTTTTTTTT#"
    , "##TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT#"
    , "###############################################"
    ]
    [ ('#', SnowWall)
    , ('.', SnowFloor)
    , (',', TaigaFloor)
    , ('T', SnowTree)
    , ('t', SnowTree2)
    , ('=', Ice)
    ]
