{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DeriveGeneric #-}

module Tiles
  ( Tile(..)
  , DoorClosing(..)
  , Strength(..)
  , LaserColor(..)
  , coloredDoor
  , doorColor
  , rotToTriangle
  , isPassable
  , isLaserPassable
  , isLaserReceptable
  , isOpenedDoor
  , isClosedDoor
  , isButton
  , isDoor
  , isPotentialLaserSource
  , toggleDoor
  , isBoulder
  , isBoulderPushableFloor
  , background
  , boulderToPushableFloor
  , receptableColor
  )
where

import           Control.Monad.Trans.State.Strict
import           Data.Bits
import           Data.Data
import           Data.Default.Class
import           Data.Monoid
import           GHC.Generics

import           CM.AnimatedTextIO
import           CM.LevelLike
import           CM.LevelRender
import           CM.Portal
import           CM.TextIO

data Strength = Strong | Weak
  deriving ( Eq, Ord, Show, Read, Typeable, Data, Generic, Enum )

data LaserColor = Red | Blue
  deriving ( Eq, Ord, Show, Typeable, Data, Generic, Enum )

data Tile
  = Floor
  | Tree
  | Tree2
  | TreeStump
  | Wall
  | Brook
  | FilledBrook
  | Sand
  | IndustrialWall
  | IndustrialFloor
  | WoodenWall
  | WoodenFloor
  | Button
  | ButtonGreen
  | OpenedDoor
  | ClosedDoor
  | OpenedDoorGreen
  | ClosedDoorGreen
  | BoulderPushableFloor
  | Boulder
  | BoulderPushableFloorGreen
  | BoulderGreen
  | Glass
  | EnergyModule
  | ChompTile
  | ChompTile20
  | RespawnPoint
  | Path
  | RedLaser !TileOrientation
  | BlueLaser !TileOrientation
  | InactiveLaser !TileOrientation
  | RedLaserReceptable !DoorClosing
  | BlueLaserReceptable !DoorClosing
  | RedDoor !DoorClosing
  | BlueDoor !DoorClosing
  | EnergyDoor
  | TaigaFloor
  | TaigaWall
  | TaigaTree
  | TaigaTree2
  | TaigaBush
  | SnowWall
  | SnowFloor
  | SnowTree
  | SnowTree2
  | Ice
  deriving ( Eq, Ord, Show, Read, Typeable, Data, Generic )

data DoorClosing = Closed | Opened
  deriving ( Eq, Ord, Show, Read, Typeable, Data, Generic )

instance Default Tile where
  {-# INLINE def #-}
  def = Floor

-- | Obstacle in the sense "can I see through it?"
instance Obstacle Tile where
  isObstacle Floor = 0
  isObstacle Wall = 10
  isObstacle Tree = 3
  isObstacle Tree2 = 3
  isObstacle TreeStump = 0
  isObstacle Brook = 0
  isObstacle Sand = 0
  isObstacle WoodenWall = 10
  isObstacle IndustrialWall = 10
  isObstacle IndustrialFloor = 0
  isObstacle Button = 0
  isObstacle ButtonGreen = 0
  isObstacle OpenedDoor = 0
  isObstacle ClosedDoor = 0
  isObstacle OpenedDoorGreen = 0
  isObstacle ClosedDoorGreen = 0
  isObstacle BoulderPushableFloor = 0
  isObstacle Boulder = 0
  isObstacle BoulderPushableFloorGreen = 0
  isObstacle BoulderGreen = 0
  isObstacle FilledBrook = 0
  isObstacle Glass = 0
  isObstacle EnergyModule = 0
  isObstacle ChompTile = 0
  isObstacle ChompTile20 = 0
  isObstacle RespawnPoint = 0
  isObstacle Path = 0
  isObstacle WoodenFloor = 0
  isObstacle RedDoor{} = 0
  isObstacle BlueDoor{} = 0
  isObstacle RedLaser{} = 0
  isObstacle BlueLaser{} = 0
  isObstacle RedLaserReceptable{} = 0
  isObstacle BlueLaserReceptable{} = 0
  isObstacle InactiveLaser{} = 0
  isObstacle EnergyDoor = 0
  isObstacle TaigaWall = 10
  isObstacle TaigaTree = 5
  isObstacle TaigaTree2 = 5
  isObstacle TaigaFloor = 0
  isObstacle TaigaBush = 0
  isObstacle SnowFloor = 0
  isObstacle SnowWall = 10
  isObstacle SnowTree = 5
  isObstacle SnowTree2 = 5
  isObstacle Ice = 0

{-# INLINABLE background #-}
background :: Tile -> Color3
background Boulder                   = Color3 0 5 20
background BoulderGreen              = Color3 0 10 0
background OpenedDoor                = Color3 0 5 20
background ClosedDoor                = Color3 0 5 20
background OpenedDoorGreen           = Color3 0 10 0
background ClosedDoorGreen           = Color3 0 10 0
background Button                    = Color3 0 5 20
background ButtonGreen               = Color3 0 10 0
background Floor                     = Color3 0 10 0
background IndustrialFloor           = Color3 0 5 20
background BoulderPushableFloor      = Color3 0 5 20
background BoulderPushableFloorGreen = Color3 0 10 0
background Path                      = Color3 10 25 10
background WoodenFloor               = Color3 30 20 0
background Glass                     = Color3 0 30 30
background TaigaFloor                = Color3 0 7 3
background TaigaBush                 = Color3 0 7 3
background SnowFloor                 = Color3 58 58 58
background Ice                       = Color3 20 50 50
background _                         = Color3 0 0 0

coloredDoor :: LaserColor -> Bool -> Tile
coloredDoor Red  True  = RedDoor Opened
coloredDoor Red  False = RedDoor Closed
coloredDoor Blue True  = BlueDoor Opened
coloredDoor Blue False = BlueDoor Closed

doorColor :: Tile -> Maybe LaserColor
doorColor RedDoor{}  = Just Red
doorColor BlueDoor{} = Just Blue
doorColor _          = Nothing

receptableColor :: Tile -> LaserColor
receptableColor RedLaserReceptable{} = Red
receptableColor BlueLaserReceptable{} = Blue
receptableColor _ = error "receptableColor: not a receptable"

isButton :: Tile -> Bool
isButton Button      = True
isButton ButtonGreen = True
isButton _           = False

isOpenedDoor :: Tile -> Bool
isOpenedDoor OpenedDoor      = True
isOpenedDoor OpenedDoorGreen = True
isOpenedDoor _               = False

isClosedDoor :: Tile -> Bool
isClosedDoor ClosedDoor      = True
isClosedDoor ClosedDoorGreen = True
isClosedDoor _               = False

isDoor :: Tile -> Bool
isDoor OpenedDoor      = True
isDoor OpenedDoorGreen = True
isDoor ClosedDoor      = True
isDoor ClosedDoorGreen = True
isDoor _               = False

toggleDoor :: Tile -> Tile
toggleDoor OpenedDoor      = ClosedDoor
toggleDoor OpenedDoorGreen = ClosedDoorGreen
toggleDoor ClosedDoor      = OpenedDoor
toggleDoor ClosedDoorGreen = OpenedDoorGreen
toggleDoor _               = error "toggleDoor: not a door."

isPotentialLaserSource :: Tile -> Bool
isPotentialLaserSource RedLaser{}      = True
isPotentialLaserSource BlueLaser{}     = True
isPotentialLaserSource InactiveLaser{} = True
isPotentialLaserSource _               = False

isBoulder :: Tile -> Bool
isBoulder Boulder         = True
isBoulder BoulderGreen    = True
isBoulder RedLaser{}      = True
isBoulder BlueLaser{}     = True
isBoulder InactiveLaser{} = True
isBoulder _               = False

boulderToPushableFloor :: Tile -> Tile
boulderToPushableFloor Boulder         = BoulderPushableFloor
boulderToPushableFloor BoulderGreen    = BoulderPushableFloorGreen
boulderToPushableFloor RedLaser{}      = BoulderPushableFloor
boulderToPushableFloor BlueLaser{}     = BoulderPushableFloor
boulderToPushableFloor InactiveLaser{} = BoulderPushableFloor
boulderToPushableFloor _ = error "boulderToPushableFloor: not a boulder"

isBoulderPushableFloor :: Tile -> Bool
isBoulderPushableFloor BoulderPushableFloorGreen = True
isBoulderPushableFloor BoulderPushableFloor      = True
isBoulderPushableFloor _                         = False

isLaserReceptable :: Tile -> Bool
isLaserReceptable RedLaserReceptable{}  = True
isLaserReceptable BlueLaserReceptable{} = True
isLaserReceptable _                     = False

isLaserPassable :: Tile -> Bool
isLaserPassable tile =
  not (isLaserReceptable tile)
    && not (isBoulder tile)
    && (isPassable tile || tile == Glass)

isPassable :: Tile -> Bool
isPassable tile =
  isObstacle tile
    == 0
    && tile
    /= Brook
    && tile
    /= Glass
    && not (isClosedDoor tile)
    && tile
    /= RedDoor Closed
    && tile
    /= BlueDoor Closed

instance CharToTile Tile where
  charToTile '#' = Wall
  charToTile '.' = Floor
  charToTile 'T' = Tree
  charToTile 't' = Tree2
  charToTile '_' = TreeStump
  charToTile '~' = Brook
  charToTile ',' = Sand
  charToTile '=' = ClosedDoor
  charToTile '+' = Button
  charToTile ':' = BoulderPushableFloor
  charToTile ';' = Path
  charToTile 'O' = Boulder
  charToTile '%' = Glass
  charToTile 'x' = EnergyModule
  charToTile '1' = RespawnPoint
  charToTile '5' = ChompTile
  charToTile '6' = ChompTile20
  charToTile 'P' = WoodenWall
  charToTile '^' = EnergyDoor
  charToTile ch = error $ "Unknown tile character: " <> show ch

instance Monad m => TileToRenderedTile Tile (AnimatedTile m (Attributes, Char)) where
  {-# INLINEABLE toRenderedTile #-}
  toRenderedTile Floor visible = static (visibilize visible $ colorsToAttributes (Color3 0 0 0) (Color3 0 10 0), ' ')
  toRenderedTile Wall visible = static (visibilize visible $ colorsToAttributes (Color3 0 0 0) (Color3 30 50 20), ' ')
  toRenderedTile Tree visible = static (visibilize visible $ colorsToAttributes (Color3 10 50 20) (Color3 0 10 0), '♣')
  toRenderedTile Tree2 visible = static (visibilize visible $ colorsToAttributes (Color3 5 25 10) (Color3 0 10 0), '♣')
  toRenderedTile TreeStump visible = static (visibilize visible $ colorsToAttributes (Color3 35 20 10) (Color3 0 10 0), '■')
  toRenderedTile Brook visible = static (visibilize visible $ colorsToAttributes (Color3 10 30 60) (Color3 0 5 25), '~')
  toRenderedTile FilledBrook visible = static (visibilize visible $ colorsToAttributes (Color3 0 0 0) (Color3 20 10 25), ' ')
  toRenderedTile Sand visible = static (visibilize visible $ colorsToAttributes (Color3 0 0 0) (Color3 60 40 5), ' ')
  toRenderedTile IndustrialWall visible = static (visibilize visible $ colorsToAttributes (Color3 0 0 0) (Color3 10 30 60), ' ')
  toRenderedTile IndustrialFloor visible = static (visibilize visible $ colorsToAttributes (Color3 0 0 0) (Color3 0 5 20), ' ')
  toRenderedTile Button visible = animated $ \_ secs -> return $
    if (floor secs :: Int) `mod` 2 == 0
      then (visibilize visible $ colorsToAttributes (Color3 63 63 0) (Color3 0 5 20), '•')
      else (visibilize visible $ colorsToAttributes (Color3 63 63 0) (Color3 0 5 50), '•')
  toRenderedTile ButtonGreen visible = animated $ \_ secs -> return $
    if (floor secs :: Int) `mod` 2 == 0
      then (visibilize visible $ colorsToAttributes (Color3 63 63 0) (Color3 0 10 0), '•')
      else (visibilize visible $ colorsToAttributes (Color3 63 63 0) (Color3 0 40 0), '•')
  toRenderedTile ClosedDoor visible = static (visibilize visible $ colorsToAttributes (Color3 63 63 0) (Color3 0 5 20), '≡')
  toRenderedTile OpenedDoor visible = static (visibilize visible $ colorsToAttributes (Color3 63 63 0) (Color3 0 5 20), '-')
  toRenderedTile ClosedDoorGreen visible = static (visibilize visible $ colorsToAttributes (Color3 63 63 0) (Color3 0 10 0), '≡')
  toRenderedTile OpenedDoorGreen visible = static (visibilize visible $ colorsToAttributes (Color3 63 63 0) (Color3 0 10 0), '-')
  toRenderedTile BoulderPushableFloor visible = static
    (visibilize visible $ colorsToAttributes (Color3 63 63 63) (Color3 0 5 20), '·')
  toRenderedTile Boulder visible = static
    (visibilize visible $ colorsToAttributes (Color3 43 43 43) (Color3 0 5 20), '■')
  toRenderedTile BoulderPushableFloorGreen visible = static
    (visibilize visible $ colorsToAttributes (Color3 63 63 63) (Color3 0 10 0), '·')
  toRenderedTile BoulderGreen visible = static
    (visibilize visible $ colorsToAttributes (Color3 43 43 43) (Color3 0 10 0), '■')
  toRenderedTile Glass visible = static
    (visibilize visible $ colorsToAttributes (Color3 0 50 50) (Color3 0 30 30), 'Ξ')
  toRenderedTile EnergyModule visible = animated $ \_ secs -> return $
    let r1 = floor $ (sin secs + 1.0) * 0.5 * 63.0
        b1 = floor $ (sin (secs + 1.0) + 1.0) * 0.5 * 63.0
     in (visibilize visible $ colorsToAttributes (Color3 r1 0 b1) (Color3 0 10 0), '⎈')
  toRenderedTile ChompTile _visible = static $ (colorsToAttributes (Color3 63 63 63) (Color3 0 0 0), '?')
  toRenderedTile ChompTile20 _visible = static $ (colorsToAttributes (Color3 63 63 63) (Color3 0 0 0), '?')
  toRenderedTile RespawnPoint visible = static
    (visibilize visible $ colorsToAttributes (Color3 0 63 0) (Color3 0 30 0), '◙')
  toRenderedTile Path visible = static
    (visibilize visible $ colorsToAttributes (Color3 0 0 0) (Color3 10 25 10), ' ')
  toRenderedTile WoodenWall visible = static
    (visibilize visible $ colorsToAttributes (Color3 50 20 0) (Color3 40 20 0), '≡')
  toRenderedTile WoodenFloor visible = static
    (visibilize visible $ colorsToAttributes (Color3 0 0 0) (Color3 15 7 0), ' ')
  toRenderedTile (RedLaser rot) visible = static
    (visibilize visible $ colorsToAttributes (Color3 63 0 0) (Color3 43 43 43), rotToTriangle rot)
  toRenderedTile (BlueLaser rot) visible = static
    (visibilize visible $ colorsToAttributes (Color3 0 0 63) (Color3 43 43 43), rotToTriangle rot)
  toRenderedTile (RedLaserReceptable Opened) visible = static
    (visibilize visible $ colorsToAttributes (Color3 63 0 0) (Color3 33 23 23), '✠')
  toRenderedTile (BlueLaserReceptable Opened) visible = static
    (visibilize visible $ colorsToAttributes (Color3 0 0 63) (Color3 23 23 33),  '✠')
  toRenderedTile (RedLaserReceptable Closed) visible = static
    (visibilize visible $ colorsToAttributes (Color3 33 0 0) (Color3 27 23 23), '✠')
  toRenderedTile (BlueLaserReceptable Closed) visible = static
    (visibilize visible $ colorsToAttributes (Color3 0 0 33) (Color3 23 23 27),  '✠')
  toRenderedTile (InactiveLaser rot) visible = static
    (visibilize visible $ colorsToAttributes (Color3 20 20 20) (Color3 43 43 43), rotToTriangle rot)
  toRenderedTile (RedDoor Closed) visible = static
    (visibilize visible $ colorsToAttributes (Color3 63 0 0) (Color3 40 0 0),  '#')
  toRenderedTile (RedDoor Opened) visible = static
    (visibilize visible $ colorsToAttributes (Color3 30 0 0) (Color3 10 0 0),  '.')
  toRenderedTile (BlueDoor Closed) visible = static
    (visibilize visible $ colorsToAttributes (Color3 0 0 63) (Color3 0 0 40),  '#')
  toRenderedTile (BlueDoor Opened) visible = static
    (visibilize visible $ colorsToAttributes (Color3 0 0 30) (Color3 0 0 10),  '.')
  toRenderedTile EnergyDoor visible = static
    (visibilize visible $ colorsToAttributes (Color3 63 63 63) (Color3 20 20 20), '⎈')
  toRenderedTile TaigaFloor visible = static
    (visibilize visible $ colorsToAttributes (Color3 0 0 0) (Color3 0 7 3), ' ')
  toRenderedTile TaigaTree visible = static
    (visibilize visible $ colorsToAttributes (Color3 0 25 9) (Color3 0 7 3), '♠')
  toRenderedTile TaigaTree2 visible = static
    (visibilize visible $ colorsToAttributes (Color3 0 20 9) (Color3 0 7 3), '♠')
  toRenderedTile TaigaWall visible = static
    (visibilize visible $ colorsToAttributes (Color3 0 0 0) (Color3 0 20 10), ' ')
  toRenderedTile TaigaBush visible = static
    (visibilize visible $ colorsToAttributes (Color3 0 25 9) (Color3 0 7 3), '.')
  toRenderedTile SnowWall visible = static
    (visibilize visible $ colorsToAttributes (Color3 0 0 0) (Color3 63 63 63), ' ')
  toRenderedTile SnowFloor visible = static
    (visibilize visible $ colorsToAttributes (Color3 0 0 0) (Color3 58 58 58), ' ')
  toRenderedTile SnowTree visible = static
    (visibilize visible $ colorsToAttributes (Color3 0 25 14) (Color3 58 58 58), '♠')
  toRenderedTile SnowTree2 visible = static
    (visibilize visible $ colorsToAttributes (Color3 0 20 14) (Color3 58 58 58), '♠')
  toRenderedTile Ice visible = static
    (visibilize visible $ colorsToAttributes (Color3 30 60 60) (Color3 20 50 50), '░')

rotToTriangle :: TileOrientation -> Char
rotToTriangle OnRight  = '▶'
rotToTriangle OnLeft   = '◀'
rotToTriangle OnTop    = '▲'
rotToTriangle OnBottom = '▼'

{-# INLINE visibilize #-}
visibilize :: Obscuring -> Attributes -> Attributes
visibilize Visible atts = atts
visibilize Obscured atts =
  let (Color3 fr fg fb, Color3 br bg bb) = attributesToColors atts
  in  colorsToAttributes (Color3 (fr `div` 2) (fg `div` 2) (fb `div` 2))
                         (Color3 (br `div` 2) (bg `div` 2) (bb `div` 2))
