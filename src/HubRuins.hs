{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}

module HubRuins
  ( hubRuins
  , preprocess
  )
where

import           Control.Monad.State.Strict
import           Data.Foldable
import qualified Data.Map.Strict               as M
import qualified Data.Set                      as S

import           CM.Coords
import           CM.LevelLike
import           CM.Portal
import           CM.TileWorld

import           GameState
import           ForestLasersPuzzle
import           LasorPuzzle1
import           LasorPuzzle2
import           Lasers
import           RuinsToTaiga
import           Tiles

hubRuins :: TileWorld Tile
hubRuins = worldFromBuilder $ do
  portal_group <- makePortalGroup
    [ ('a', OnLeft)
    , ('A', OnRight)
    , ('b', OnRight)
    , ('B', OnLeft)
    , ('C', OnTop)
    , ('c', OnBottom)
    , ('D', OnTop)
    , ('d', OnBottom)
    , ('D', OnTop)
    , ('d', OnBottom)
    , ('E', OnTop)
    , ('e', OnBottom)
    , ('g', OnLeft)
    , ('G', OnRight)
    , ('h', OnRight)
    , ('H', OnLeft)
    , ('i', OnBottom)
    , ('I', OnTop)
    , ('j', OnTop)
    , ('J', OnBottom)
    ]
  lasor_connector2 <- makePortalGroup [('w', OnBottom), ('W', OnTop)]
  forest_lasers    <- makePortalGroup [('z', OnRight), ('Z', OnLeft)]
  passage1         <- makePortalGroup
    [ ('k', OnTop)
    , ('l', OnTop)
    , ('m', OnTop)
    , ('K', OnBottom)
    , ('L', OnBottom)
    , ('M', OnBottom)
    ]
  putLevel
    [portal_group, lasor_connector2, forest_lasers, passage1]
    [ "####################################################################"
    , "###################T###############..H##############################"
    , "#####TTTTTTTTTTTTT#t#TTTTTTTTTTTTT#.###TTTTTTTTTTTTTTTTTTTTTTTTTT#w#"
    , "z...#TTTTTTTTTTTTTTTtTTTTTTTTTTTT##.TTTTTTTTTTTTTTTTTTtTTTTTTTTTT#.#"
    , "###.#TtTtTTTTTTTTT..tTTTTTTTTTTTT##.TTTTTTTTTTTTTTTTTTtTTTTTTTTTTT.#"
    , "#TT.TTTTTtttTTTTT...tTTTTTTTTTTTT##.TTTTTTTTTTTTTTTTTTtTTTTTTTTTTT.#"
    , "#TT............t....TT.....tTt.TT##.TTTTTTT...TTTTTTTTtTTTTTTTTTTT.#"
    , "#TTT.....T.............tT......tT##...T............TTTtTTTTTTTT....#"
    , "#t....TT...T...............T.....##~~........................Tt.T###"
    , "#T...........................T....#T~.................;;;;...;;...T#"
    , "#tt.................................~~................;#;#........T#"
    , "#TT.................................,~,,,.............;#D#.....t..T#"
    , "#T.#####............................,~~~,.............;###........T#"
    , "#t.###A;;;;;;;.......................~~~,,.....;;;;;;;;;;;;;;;;;;;;#"
    , "#T.#####;....;;;;;;;;;;;;;;;;;;.......~~~,.....;..................T#"
    , "#t............................;;;.....~~~,.....;....PPPPPPPP...T..T#"
    , "#TT......................T......;;;............;....PPPPPPPP......T#"
    , "#Ttt..............................;;;;;;;;;;;;;;;;;;PPiPPPPP...T..T#"
    , "#TT................................................;PP;PPPPP......T#"
    , "#T..........................tT..ttTT...............;;;;;;;.....T..T#"
    , "#T...xxxxxxx....................tttTTTTT......t....t.......TT..TTTT#"
    , "#TT.....T..................tT...Tt#TTTTTTTTT....tTTtT......TTT.TTTT#"
    , "#Tt..t....T..#^%.............TT.T##TTTTTTTTTTTtttTTtTT....TTTT.TTTT#"
    , "#TTtTTTTTtTTt#^%......TTTTtTTTT.T##TTTTTTTTTTTtttTTtTTTttTTTTTTTTTT#"
    , "#TTTTTTTTTTTt#^%.x.#TttTTTTTTTT.T##TTTTTTTTTTTtttTTtTTTttTTTTTTTTTT#"
    , "#TTTTTTTTTTTT#^%...#TTTTTTTTTTT.T##TTTTTTTTTTTtttTTtTTTttTTTTTTTTTT#"
    , "#TTTTTTTTTTTT#^%...#TTTTTTT.tTT.T##TTTTTTTTTTTtttTTtTTTttTTTTTTTTTT#"
    , "#TTTTTTTTTTTT#^%...#TTTTTtT.....T##TTTTTTTTTTTtttTTtTTTttTTTTTTTTTT#"
    , "#TTTTTTTTTTTT#^#%%##TTTTTTt..TT.T##TTTTTTTTTTTtttTTtTTTttTTTTTTTTTT#"
    , "#TTTTTTTTTTTT#.....#TTTTTTT...#.###TTTTTTTTTTTtttTTtTTTttTTTTTTTTTT#"
    , "###############...#############.####################################"
    , "###############klm#############C####################################"
    ]
  putLevelOverrides
    [portal_group]
    [ "#####################"
    , "#..............######"
    , "#.....:::::::..######"
    , "#.....:::::::..#...##"
    , "#.....:::O:::::~...##"
    , "#.....:::::::..#...##"
    , "#.....:::::::..##...#"
    , "#..............##..##"
    , "##:##############..##"
    , "##:#############...##"
    , "#...###########....##"
    , "#.....#######......##"
    , "#..##.#######..O..###"
    , "#####.#%%%%%#..O..###"
    , "#####.#%%%%%#.:::.###"
    , "##..#.#%%%%%#.:::.###"
    , "b.....#######.:::.###"
    , "##..#.#######..:..###"
    , "#############..:..###"
    , "#.###....#.##.#~#.###"
    , "#.###.##.#.##.#~#.###"
    , "#.###.##.#.##.~.~~###"
    , "#...#....#..#.~.....#"
    , "###################.#"
    , "#x#####....=.....##.#"
    , "#.#####.####...#.##.#"
    , "#.......^..#.+.#....#"
    , "##########.##########"
    , "##########.##########"
    , "##########.##########"
    , "#######x...^.a#######"
    , "#####################"
    ]
    [('#', IndustrialWall), ('.', IndustrialFloor)]
  putLevel
    [portal_group, forest_lasers]
    [ "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTTttT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTTTTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTTTtttt"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTTTTtt"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTTTttt"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTTtttTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TT...tTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TT...tttTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TT...TtTTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTT.TTttttTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTt.TTttttt"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~....TTtTTttt"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~,..tTtTtTTTtt"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~,,,..TTTTTTTTt"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~,,.############"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~,,.#"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~,..B"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~,,..#"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~,,..###"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~,,..T#######"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~,,.ttTTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~,...TTttT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~tT..ttTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~T..TTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~,TTTTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~,TTTTTttT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTtTTTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTTttt"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTT"
    , "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TTTTTT"
    ]
  putLevelOverrides
    [portal_group]
    [ "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTT###TTTTTT###TTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTT#c#TTTTTT#J#TTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTT#.##TT.....#TTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTT##.#TT.TTt..TTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTT#.#TT.TTTttTTTTTTTTTTTTTtttt"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTT#.#TT.TTTTTTTTTTTTTTTTttttttt"
    , "TTTTTTTTTTTTTTTTTTTTTTtTTT#.#tT.###TTTTTTTTTTttttttttttT"
    , "TTTTTTTTTTTTTTTTTTttttttTT#.=+..#+.....TTTTTTTTTttTTTTtttttt"
    , "TTTTTTTTTTT.....TTTttttttT..###.##%###.TTTTTTTTTttttt#tttttT"
    , "######Tt.............tt+...##......=.#.#.......#"
    , "######t...............TtTt=#...x..##.#.#.......#"
    , "######t..................tO#......#..#.........#"
    , "######....................O#......#.####.......#"
    , "##############.OOOOOOOOOOO:OOOOOOOOOO:.######.##"
    , "#######......###.........#.#..................##"
    , "######...................#.#...................#"
    , "######..................##.##..................#"
    , "######........................................T#"
    , "######t..................####......t..T......t##"
    , "TTTTTTTTTT.....tTTtTTT.T.TtTT....TTTTTTT.TT.ttTTTTTTTTT"
    , "TTTTTT#TTTTTT..TTTTTttTT..TTTTTTTTTTTTtTtTTtTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTtTTTTTTtTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTttttTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTtTTtTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    ]
    [ ('O', BoulderGreen)
    , ('+', ButtonGreen)
    , ('=', ClosedDoorGreen)
    , (':', BoulderPushableFloorGreen)
    ]
  putLevelOverrides
    [portal_group]
    [ "#################################"
    , "###################.............#"
    , "###################R###%%%%%%%%.#"
    , "#1..................%...%.=.....#"
    , "###.#.........:.:..r%...%%%888..#"
    , "###j#.........:.)...%.5.888888..#"
    , "#####.........:.:..7%x..888888..#"
    , "###d#.........:.:...%..%%%%888..#"
    , "###.#.........>.:...%..%+.......#"
    , "#...%.........:.:..r###%%%%%%%%.#"
    , "#...%...............R.#.........#"
    , "#...%...............#.#.#########"
    , "#...%...............#...#"
    , "#...%%%%%%%%%%%%###%#####"
    , "#...............#e#.#"
    , "#...............#.#.#"
    , "#...................#"
    , "#####################"
    ]
    [ ('#', IndustrialWall)
    , ('.', IndustrialFloor)
    , ('>', RedLaser OnRight)
    , (')', BlueLaser OnRight)
    , ('r', RedLaserReceptable Closed)
    , ('7', BlueLaserReceptable Closed)
    , ('R', RedDoor Closed)
    , ('8', BlueDoor Closed)
    ]
  putLevelOverrides
    [portal_group]
    [ "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTT###TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTT....g#TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTT.TT###TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTT.TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTT.TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTT.TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTT#....#TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTT#....#...........TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTT#....#::::::::::.T..1..TTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTT=TTT:::TOOOOO:.T.....TTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "######:::::T5:::OOOOO:........................5..#"
    , "######::::::::::OOOOO:...........................#"
    , "######:::::%%%%%%%%%%%%%%#.#%%%%%%%%%%%%%%%%.....#"
    , "######:::::%.........T...#E#~~....t......%%%.....#"
    , "######.....%....t........###~~...........%~......#"
    , "######.....%....~~~........~~..t.........%%%%....#"
    , "######.....%.....~~T...t.................%%%%%%%=#"
    , "######..+..%..t...........T.......T........%%%...#"
    , "######.....%................................+%x..#"
    , "TTTTTTTTTTTTTT..t.TT.TT..T..T.TT...TTT###.##TTTTTTTTTTT"
    , "TTTTTTTTTTTTTT.TT.TTtTTtTTTTT.T.TTTTTT#G..#TTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT#####TTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    ]
    [ ('O', BoulderGreen)
    , ('+', ButtonGreen)
    , ('=', ClosedDoorGreen)
    , (':', BoulderPushableFloorGreen)
    ]
  putLevel
    [portal_group]
    [ "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT....................#.#......................TTTTT"
    , "TTTTT.....................6.......................TTTTT"
    , "TTTTT....................#.#......................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.....................x.......................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.....1.......................................TTTTT"
    , "TTTTT...###.......................................TTTTT"
    , "TTTTT...#h........................................TTTTT"
    , "TTTTT...###.......................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTT.............................................TTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    , "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
    ]
  putLevelOverrides
    [portal_group]
    [ "PPPPPPPPPPPPPPPPPPPPPPPPP"
    , "P.......................P"
    , "P.......................P"
    , "P.......................P"
    , "P.......................P"
    , "P.......................P"
    , "P.......................P"
    , "P.......................P"
    , "PPIPPPPPPPPPPPPPPPPPPPPPP"
    ]
    [('.', WoodenFloor)]
  lasorPuzzle2 lasor_connector2
  forestLasersPuzzle forest_lasers
  ruinsToTaiga passage1

preprocess :: GameState -> TileWorld Tile -> GameState
preprocess gs world = chainTilesToChainChomps gs world

chainTilesToChainChomps :: GameState -> TileWorld Tile -> GameState
chainTilesToChainChomps gs world = execState replacer gs
 where
  places   = listBlocks world :: [(WorldCoords2D, Tile)]

  replacer = do
    chompiesL .= M.empty
    chompChainsL .= M.empty
    respawnPointsL .= M.empty
    lasersL .= emptyLaserState

    -- Recount number of energy modules in the game
    totalEnergyModulesL .= 0
    for_ places
      $ \(_, tile) -> when (tile == EnergyModule) $ totalEnergyModulesL += 1

    -- Remove energy modules that were already collected
    for_ places $ \(coords@(WorldCoords2D level_key _), tile) ->
      when
          (tile == EnergyModule && gs ^. collectedEnergyModuleLevelsL level_key)
        $  worldL
        .  tileAtL coords
        .= Floor

    for_ (filter (\place -> place ^. _2 `elem` [ChompTile, ChompTile20]) places)
      $ \(coords, tile) -> do
          let scoords = toSimpleCoords coords
          worldL . tileAtL coords .= Floor
          chompiesL . at scoords .= Just
            (initialChomp scoords $ if tile == ChompTile then 8 else 15)
    for_ (filter (\place -> place ^. _2 == RespawnPoint) places)
      $ \(coords, _tile) -> do
          let WorldSimpleCoords2D level_key scoords = toSimpleCoords coords
          respawnPointsL . at level_key .= Just scoords
    for_ places $ \place -> case place ^. _2 of
      RedLaser ori -> do
        laserAtGS (toSimpleCoords $ place ^. _1) .= (Just (Red, ori))
      BlueLaser ori ->
        laserAtGS (toSimpleCoords $ place ^. _1) .= (Just (Blue, ori))
      _ -> return ()
    for_ places $ \place -> do
      let WorldCoords2D level_key coords = place ^. _1
      case place ^. _2 of
        RedLaserReceptable{} -> laserReceptablesL . at level_key %= \case
          Nothing  -> Just $ S.singleton (toCoords2D coords)
          Just set -> Just $ S.insert (toCoords2D coords) set
        BlueLaserReceptable{} -> laserReceptablesL . at level_key %= \case
          Nothing  -> Just $ S.singleton (toCoords2D coords)
          Just set -> Just $ S.insert (toCoords2D coords) set
        _ -> return ()
