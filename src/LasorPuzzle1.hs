{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}

module LasorPuzzle1
  ( lasorPuzzle1
  )
where

import           CM.Coords
import           CM.Portal
import           CM.LevelLike
import           CM.TileWorld

import           Tiles

lasorPuzzle1
  :: LevelLike (Level w) Coords2D Tile => PortalGroup -> WBuilder w ()
lasorPuzzle1 pgroup = do
  putLevelOverrides
    [pgroup]
    [ "###################################################"
    , "###################################################"
    , "#....................%....#..............#........#"
    , "#.............v::::..%..b.#...::::::::...#........#"
    , "#.............::>::..######...:......:...##########"
    , "#.............:::::.....R.....:......:......R..x..#"
    , "#.............:::::.....RB....:......:...r..R.....#"
    , "#.............:>::>..######...:V::::.:::.##########"
    , "#.#...........:::::..%..r.#..............#........#"
    , "#Y#..................%....#..............#........#"
    , "##############%%%##########%%%%%%BB%b%%%%##########"
    , "#............#...#...#....#..............#"
    , "#.:>::.......#...#...#....#..::::::::::..#"
    , "#.::w:.......#...#...######..::::::::::..#"
    , "#.::::.......................::::::::::..#"
    , "#.:::::::::::::::::::::::::::::::::::::..#"
    , "#.::::::::Uu:::::::::::::::::::::::::::..#"
    , "#........................................#"
    , "##########################################"
    ]
    [ ('#', IndustrialWall)
    , ('.', IndustrialFloor)
    , ('v', RedLaser OnBottom)
    , ('b', BlueLaserReceptable Closed)
    , ('B', BlueDoor Closed)
    , ('r', RedLaserReceptable Closed)
    , ('R', RedDoor Closed)
    , ('V', BlueLaser OnBottom)
    , (')', BlueLaser OnRight)
    , ('>', InactiveLaser OnRight)
    , ('w', InactiveLaser OnTop)
    , ('u', RedLaser OnTop)
    , ('U', BlueLaser OnTop)
    ]
