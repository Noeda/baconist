{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE CPP #-}

module Main where

import           Control.Applicative
import           Control.Concurrent
import           Control.Concurrent.STM
import           Control.Exception
import           Control.Monad
import           Control.Monad.IO.Class
import           Control.Monad.State.Strict
import           Control.Monad.Trans.Class
import           Data.Data
import           Data.Default.Class
import           Data.Foldable
import qualified Data.IntSet                   as IS
import qualified Data.Map.Strict               as M
import           Data.Maybe
import           Data.Monoid
import           Data.Ord
import qualified Data.OrdPSQ                   as PQ
import           Data.String
import qualified Data.Text                     as T
import           GHC.Generics
import           System.Environment
import           System.Exit
import           System.IO

import           Debug.Trace

import           CM.AnimatedTextIO
import           CM.ImapLevel
#ifndef GHCJS
import           CM.HGameLauncher
#endif
import           CM.KeyInput
import           CM.PortableTextIO
import           CM.LevelRender
import           CM.Portal
import           CM.TileWorld

import           GameState
import           GameTick
import           HubRuins
import           Lasers
import           Tiles

initialWorld :: (TileWorld Tile, WorldCoords2D)
initialWorld =
  let w = hubRuins
  in  ( w
      , WorldCoords2D
        2
        (SwizzCoords2D (Coords2D 58 14) Rot0 NoSwizzle NoSwizzle)
      )

newtype GameStateView = GameStateView GameState
  deriving ( Eq, Show, Typeable, Generic )

instance EntityView GameStateView WorldCoords2D (AnimatedTile m (Attributes, Char)) where
  -- Todo: flatten with Alternative or something
  viewEntity world_coords (GameStateView game_state) = static <$>
    if toSimpleCoords world_coords == toSimpleCoords (playerPosition game_state)
      then let Color3 br bg bb = background tile
            in Just (colorsToAttributes (Color3 33 33 33) (Color3 (max 5 br-5) (max 5 bg-5) (max 5 bb - 5)), '@')
      else case game_state^.chompiesL.at (toSimpleCoords world_coords) of
             Nothing -> case game_state^.chompChainsL.at (toSimpleCoords world_coords) of
               Just chain | not $ isBoulder tile || tile == EnergyModule || tile == ClosedDoor || tile == ClosedDoorGreen
                 -> Just (colorsToAttributes (Color3 30 30 30) (background tile), '∞')
               _ -> laserCheck <|> snowStepCheck
             Just chomp -> Just (colorsToAttributes (Color3 40 40 40) (background tile), 'Σ')
   where
    tile = tileAt (world game_state) world_coords

    snowStepCheck = if hasSnowStep (toSimpleCoords world_coords) game_state
      then Just (colorsToAttributes (Color3 0 0 0) (Color3 58 58 58), '.')
      else Nothing

    laserCheck = case laserStatusAt (game_state^.lasersL) (toSimpleCoords world_coords) of
      NoLaser -> Nothing
      LaserCollision -> Just (colorsToAttributes (Color3 40 0 40) (background tile), '☼')
      LaserAt lcolor ori strength ->
        let col = case (lcolor, strength) of
                    (Red, Strong) -> Color3 63 0 0
                    (Blue, Strong) -> Color3 0 0 63
                    (Red, Weak) -> Color3 33 0 0
                    (Blue, Weak) -> Color3 0 0 33
            ch = case (ori, strength) of
                   (OnRight, Strong) -> '═'
                   (OnLeft, Strong) -> '═'
                   (OnTop, Strong) -> '║'
                   (OnBottom, Strong) -> '║'
                   (OnRight, Weak) -> '─'
                   (OnLeft, Weak) -> '─'
                   (OnTop, Weak) -> '│'
                   (OnBottom, Weak) -> '│'
          in case tile of
               InactiveLaser ori ->
                 Just (colorsToAttributes col (Color3 43 43 43), rotToTriangle ori)
               _ | not (isLaserPassable tile) -> Nothing
               _ -> Just (colorsToAttributes col (background tile), ch)

initialGameState :: GameState
initialGameState = preprocess
  (GameState
    { world                       = fst initialWorld
    , originalLevelStates         = fst initialWorld
    , playerPosition              = snd initialWorld
    , respawnPoints               = M.empty
    , collectedEnergyModules      = 0
    , usedEnergyModules           = 0
    , chompies                    = M.empty
    , chompChains                 = M.empty
    , dead                        = False
    , npcs                        = M.empty
    , lasers                      = emptyLaserState
    , laserReceptables            = M.empty
    , frozenCamera                = Nothing
    , collectedEnergyModuleLevels = IS.empty
    , totalEnergyModules          = 0
    , turn                        = 0
    , snowSteps                   = PQ.empty
    }
  )
  (fst initialWorld)

main :: IO ()
main = do
#ifndef GHCJS
  args <- getArgs
  case args of
    [] -> mainNative
    ["server", host, port] ->
      runHGameLauncherServer (T.pack host) (read port) $ \key_input ->
        runAnimatedTextIO $ flip evalStateT initialGameState (start key_input)
    ["client", host, port] -> runHGameLauncherClient (T.pack host) (read port)
    _                      -> do
      hPutStrLn stderr "Usage examples:"
      hPutStrLn stderr ""
      hPutStrLn stderr
                "baconist                      -- This just runs Baconist"
      hPutStrLn stderr
                "baconist server 0.0.0.0 8080  -- Start a Baconist server"
      exitFailure
#else
  mainNative
#endif

mainNative :: IO ()
mainNative = do
  key_input <- newTChanIO
  void $ forkIO $ forever $ waitForKey >>= atomically . writeTChan key_input
  withPortableTextIO $ runAnimatedTextIO $ flip evalStateT
                                                initialGameState
                                                (start key_input)

start
  :: ( MonadIO m
     , TextIO m
     , TileToRenderedTile Tile (AnimatedTile m tile)
     , EntityView GameStateView WorldCoords2D (AnimatedTile m tile)
     , TiledRenderer m tile
     )
  => TChan Key
  -> StateT GameState (AnimatedTextIO m tile) ()
start input_key = go
 where
  go = do
    modify tickGameState
    game_state    <- get
    terminal_size <- lift terminalSize
    new_world     <-
      lift
      $ dontFlushWith
      $ withDOffset (toCoords2D $ playerPosition game_state)
      $ do
          clear
          let view =
                (relativeRenderView
                  (case frozenCamera game_state of
                    Nothing  -> playerPosition game_state
                    Just pos -> pos
                  )
                  (Coords2D 0 1)
                  ((\(Coords2D w h) -> Coords2D w (h - 1)) terminal_size)
                )
          (rendered_coords, new_world) <- renderRaycastingView
            (world game_state)
            view
            (GameStateView game_state)
          renderNonVisibleTiles (world game_state) view rendered_coords
          if
            | dead game_state -> setText
              (atts (Color3 63 30 30) (Color3 10 0 0) <> "You are DEAD, R.I.P.")
              (Coords2D 0 0)
            | (game_state^.collectedEnergyModulesL - game_state^.usedEnergyModulesL) < (game_state^.totalEnergyModulesL - game_state^.usedEnergyModulesL) -> setText
              (  "Energy modules ("
              <> atts (Color3 63 0 0) (Color3 0 0 0)
              <> "⎈"
              <> atts (Color3 63 63 63) (Color3 0 0 0)
              <> ") "
              <> (  fromString
                 $  show (collectedEnergyModules game_state - game_state^.usedEnergyModulesL)
                 <> "/"
                 <> show (game_state ^. totalEnergyModulesL - game_state^.usedEnergyModulesL)
                 )
              )
              (Coords2D 0 0)
            | otherwise -> setText ("Yay :3") (Coords2D 0 0)
          return new_world
    lift flush
    modify $ \old -> old { world = new_world }
    key <- liftIO $ atomically $ readTChan input_key
    if dead game_state then restartHandling key else movementKeyhandling key

  restartHandling key = case key of
    _ -> deadMoveTowardsHome >> go

  movementKeyhandling key = case key of
#ifndef GHCJS
    KeyQ           -> return ()
#endif
    KeyZ           -> cameraFreeze >> go
    KeyH           -> goLeft >> go
    KeyJ           -> goDown >> go
    KeyK           -> goUp >> go
    KeyL           -> goRight >> go
    KeyY           -> goLeftUp >> go
    KeyU           -> goRightUp >> go
    KeyB           -> goLeftDown >> go
    KeyN           -> goRightDown >> go
    Key4           -> goLeft >> go
    Key2           -> goDown >> go
    Key8           -> goUp >> go
    Key6           -> goRight >> go
    Key7           -> goLeftUp >> go
    Key9           -> goRightUp >> go
    Key1           -> goLeftDown >> go
    Key3           -> goRightDown >> go
    KeyW           -> goUp >> go
    KeyS           -> goDown >> go
    KeyA           -> goLeft >> go
    KeyD           -> goRight >> go
    MouseClick x y -> doMouseMove x y
    _              -> go


  cameraFreeze = do
    gs <- get
    case frozenCamera gs of
      Nothing ->
        let ppos = playerPosition gs in put $ gs { frozenCamera = Just ppos }
      Just{} -> put $ gs { frozenCamera = Nothing }

  doMouseMove x y =
    let (_, f) = minimumBy
          (comparing $ \((dx, dy), _) -> (dx - x) ** 2 + (dy - y) ** 2)
          mouseMoves
    in  f >> go

  mouseMoves :: Monad m => [((Double, Double), StateT GameState m ())]
  mouseMoves =
    [ ((0, 0)    , goLeftUp)
    , ((1, 0)    , goRightUp)
    , ((1, 1)    , goRightDown)
    , ((0, 1)    , goLeftDown)
    , ((0.5, 0.0), goUp)
    , ((0.5, 1.0), goDown)
    , ((0, 0.5)  , goLeft)
    , ((1, 0.5)  , goRight)
    ]

  goLeft :: Monad m => StateT GameState m ()
  goLeft = move toLeft
  goRight :: Monad m => StateT GameState m ()
  goRight = move toRight
  goUp :: Monad m => StateT GameState m ()
  goUp = move toUp
  goDown :: Monad m => StateT GameState m ()
  goDown = move toDown
  goLeftUp :: Monad m => StateT GameState m ()
  goLeftUp = move toLeftUp
  goLeftDown :: Monad m => StateT GameState m ()
  goLeftDown = move toLeftDown
  goRightUp :: Monad m => StateT GameState m ()
  goRightUp = move toRightUp
  goRightDown :: Monad m => StateT GameState m ()
  goRightDown = move toRightDown

  move fun = modify $ \old ->
    addTurn $ leaveSnowSteps old $
    let
      !new_position@(WorldCoords2D level_key _) =
        fun (AnyTileWorld $ world old) (playerPosition old)
      !target_tile = tileAt (world old) new_position
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           -- Is there a boulder in the target position?
    in
      if
        | isBoulder target_tile -> boulderPushingMove
          old
          new_position
          new_position
          (\coords -> fun (AnyTileWorld $ world old) coords)
        | isButton target_tile -> buttonPressingMove old new_position
        | target_tile == EnergyDoor -> energyDoorMove old new_position
        | target_tile == EnergyModule -> old
          { playerPosition              = new_position
          , collectedEnergyModules      = collectedEnergyModules old + 1
          , world                       = setTile (world old) new_position Floor
          , collectedEnergyModuleLevels = IS.insert
                                            level_key
                                            (collectedEnergyModuleLevels old)
          }
        | isPassable target_tile -> old { playerPosition = new_position }
        | otherwise -> old

  -- called when pressing a button.
  buttonPressingMove (old :: GameState) new_position@(WorldCoords2D level_key _)
    =
    -- Look for a door to be triggered by the button
      case
        lookForTile new_position (world old) 1000
          $ \door_pos block -> isDoor block
      of
        Nothing -> old { playerPosition = new_position }
        Just (pos, block) ->
          updateReceptablesOnLevel level_key
            $ updateLasersAtGS (toSimpleCoords pos)
            $ (old { playerPosition = new_position
                   , world          = setTile (world old) pos (toggleDoor block)
                   }
              )

energyDoorMove :: GameState -> WorldCoords2D -> GameState
energyDoorMove gs coords
  | gs^.collectedEnergyModulesL - gs^.usedEnergyModulesL > 0
    = gs & (usedEnergyModulesL +~ 1) .
           (worldL.tileAtL coords .~ Floor) .
           (playerPositionL .~ coords)
  | otherwise = gs

boulderPushingMove
  :: GameState
  -> WorldCoords2D
  -> WorldCoords2D
  -> (WorldCoords2D -> WorldCoords2D)
  -> GameState
boulderPushingMove gs target_position player_target_position fun =
  fromMaybe gs
    $ boulderPushingMove' gs target_position player_target_position fun

boulderPushingMove'
  :: GameState
  -> WorldCoords2D
  -> WorldCoords2D
  -> (WorldCoords2D -> WorldCoords2D)
  -> Maybe GameState
boulderPushingMove' gs target_position player_target_position fun =
  fst <$> runGameState boulderPush gs
 where
  boulderPush = do
    gs <- get
    w  <- use worldL
    let !boulder_target_pos     = fun target_position
        !boulder_target_tile    = tileAt w boulder_target_pos
        !boulder                = tileAt w target_position
        !boulder_pushable_floor = boulderToPushableFloor boulder

    if
      | Just _chomp <- gs ^. chompiesL . at (toSimpleCoords boulder_target_pos)
      -> mzero
      | isBoulderPushableFloor boulder_target_tile
      -> do
        old_laser <- use $ laserAtGS target_position
        laserAtGS target_position .= Nothing
        worldL . tileAtL boulder_target_pos .= boulder
        worldL . tileAtL target_position .= boulder_pushable_floor
        modify $ updateLasersAtGS $ toSimpleCoords boulder_target_pos
        modify $ updateLasersAtGS $ toSimpleCoords target_position
        laserAtGS boulder_target_pos .= old_laser
      | isBoulder boulder_target_tile
      -> do
        gs <- get
        case
            boulderPushingMove' gs boulder_target_pos player_target_position fun
          of
            Nothing     -> mzero
            Just new_gs -> do
              put new_gs
              old_laser <- use $ laserAtGS target_position
              laserAtGS target_position .= Nothing
              worldL . tileAtL boulder_target_pos .= boulder
              worldL . tileAtL target_position .= boulder_pushable_floor
              modify $ updateLasersAtGS $ toSimpleCoords boulder_target_pos
              modify $ updateLasersAtGS $ toSimpleCoords target_position
              laserAtGS boulder_target_pos .= old_laser
      | boulder_target_tile == Brook
      -> do
        laserAtGS target_position .= Nothing
        worldL . tileAtL boulder_target_pos .= boulder_pushable_floor
        worldL . tileAtL target_position .= boulder_pushable_floor
        modify $ updateLasersAtGS $ toSimpleCoords boulder_target_pos
        modify $ updateLasersAtGS $ toSimpleCoords target_position
      | otherwise
      -> mzero

{-# INLINE leaveSnowSteps #-}
leaveSnowSteps :: GameState -> GameState -> GameState
leaveSnowSteps gs_old gs_new
 | gs_new^.playerPositionL /= gs_old^.playerPositionL &&
   gs_new^.worldL.tileAtL (gs_new^.playerPositionL) == SnowFloor =
     addSnowStep (toSimpleCoords $ gs_new^.playerPositionL) gs_new
leaveSnowSteps _ gs_new = gs_new

deadMoveTowardsHome :: MonadState GameState m => m ()
deadMoveTowardsHome = do
  gs <- get
  let scoords@(WorldSimpleCoords2D level_key coords@(Coords2D px py)) =
        toSimpleCoords (playerPosition gs)
  case gs ^? respawnPointsL . at level_key . _Just of
    Just respawn_coords
      | respawn_coords == toCoords2D (toSimpleCoords (playerPosition gs)) -> do
        let respawn_level =
              fromJust $ getLevelSnapshot level_key (originalLevelStates gs)
        worldL %= setLevelSnapshot level_key respawn_level
        deadL .= False
        modify $ \gs -> preprocess gs (originalLevelStates gs)
    Just respawn_coords@(Coords2D rx ry) -> do
      let dx = signum (rx - px)
          dy = signum (ry - py)
      modify $ \gs -> gs
        { playerPosition = WorldCoords2D
                             level_key
                             (SwizzCoords2D (coords .+ Coords2D dx dy)
                                            Rot0
                                            NoSwizzle
                                            NoSwizzle
                             )
        }
    _ -> return ()

