{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}

module GameTick
  ( tickGameState
  )
where

import           Control.Applicative
import           Control.Lens
import           Control.Monad.State.Strict
import           Data.Foldable
import           Data.Maybe

import           CM.Chain
import           CM.Coords
import           CM.LevelLike
import           CM.TileWorld

import           GameState
import           Tiles

tickGameState :: GameState -> GameState
tickGameState gs = fst $ fromMaybe (gs, ()) $ runGameState tick gs
 where
  WorldSimpleCoords2D plevel_key pcoords@(Coords2D px py) =
    toSimpleCoords $ playerPosition gs

  tick = ifor_ (gs ^. chompiesL) $ \pos (Chomp chain limit) -> do
    when (pos == toSimpleCoords (playerPosition gs)) $ deadL .= True
    let WorldSimpleCoords2D level_key coords@(Coords2D x y) = chainEnd chain
    when (level_key == plevel_key) $ do
      let dx = signum (px - x)
          dy = signum (py - y)
          pt = pursueTry limit coords chain pos
      pt dx dy <|> pt 0 dy <|> pt dx 0 <|> pure ()
   where
    pursueTry limit coords chain pos dx dy = do
      guard (dx /= 0 || dy /= 0)
      let
        target_pos  = coords .+ (Coords2D dx dy)
        target_tile = tileAt
          (world gs)
          (WorldCoords2D plevel_key
                         (SwizzCoords2D target_pos Rot0 NoSwizzle NoSwizzle)
          )
      guard (isPassable target_tile || target_tile == Brook)
      guard (not $ isBoulder target_tile)
      st <- get
      guard
        (isNothing $ st ^. chompiesL . at
          (WorldSimpleCoords2D plevel_key target_pos)
        )
      let
        new_chain' = layNewChainRestrict
          chain
          (AnyTileWorld $ world gs)
          limit
          (WorldSimpleCoords2D plevel_key target_pos)
          (\(WorldSimpleCoords2D level_key coords) -> isPassable $ tileAt
            (world gs)
            (WorldCoords2D level_key
                           (SwizzCoords2D coords Rot0 NoSwizzle NoSwizzle)
            )
          )
      guard (isJust new_chain')
      let Just new_chain = new_chain'
      chompiesL . at pos .= Nothing
      unless (target_tile == Brook)
        $  chompiesL
        .  at (WorldSimpleCoords2D plevel_key target_pos)
        .= Just (Chomp new_chain limit)
      when (target_pos == (toCoords2D $ toSimpleCoords (playerPosition gs)))
        $  deadL
        .= True
      for_ (chainToList chain) $ \pos -> chompChainsL . at pos %= \case
        Just 1 -> Nothing
        Just x -> Just (x - 1)
        _      -> Nothing
      unless (target_tile == Brook)
        $ for_ (chainToList new_chain)
        $ \pos -> chompChainsL . at pos %= Just . \case
            Nothing -> 1
            Just x  -> (x + 1)
