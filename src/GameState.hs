{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE RankNTypes #-}

module GameState
  ( GameState(..)
  , runGameState
  , hasSnowStep
  , addSnowStep
  , addTurn
  , removeOldSnowSteps
  , tileAtL
  , lasersL
  , laserAtGS
  , laserReceptablesL
  , updateLasersAtGS
  , updateReceptablesOnLevel
  , collectedEnergyModulesL
  , usedEnergyModulesL
  , collectedEnergyModuleLevelsL
  , totalEnergyModulesL
  , playerPositionL
  , worldL
  , deadL
  , respawnPointsL
  , GSM()
  , Chomp(..)
  , initialChomp
  , chompiesL
  , chompChainsL
  , Turn
  , module Control.Lens
  , module Control.Monad.State.Strict
  )
where

import           Control.Applicative
import           Control.Lens
import           Control.Monad.Trans.Except
import           Control.Monad.State.Strict
import           Data.Data
import           Data.Default.Class
import           Data.Foldable
import qualified Data.IntSet                   as IS
import qualified Data.Map.Strict               as M
import           Data.Maybe
import qualified Data.OrdPSQ                   as PQ
import qualified Data.Set                      as S
import           GHC.Generics

import           CM.Chain
import           CM.Coords
import           CM.LevelLike
import           CM.LiftLevel
import           CM.Portal
import           CM.TileWorld

import           Lasers
import           Tiles

type Turn = Int

data GameState = GameState
  -- Fields marked with underscore require special handling and should only be
  -- managed through lenses.
  { world          :: TileWorld Tile
  , originalLevelStates :: TileWorld Tile
  , respawnPoints :: M.Map Int Coords2D
  , playerPosition :: WorldCoords2D
  , chompies :: M.Map WorldSimpleCoords2D Chomp
  , chompChains :: M.Map WorldSimpleCoords2D Int
  , collectedEnergyModules :: Int
  , usedEnergyModules :: Int
  , totalEnergyModules :: Int
  , lasers :: LaserState
  , laserReceptables :: M.Map Int (S.Set Coords2D)
  , dead :: Bool
  , npcs :: M.Map WorldSimpleCoords2D NPC
  , frozenCamera :: Maybe WorldCoords2D
  , collectedEnergyModuleLevels :: IS.IntSet
  , turn :: Turn
  , snowSteps :: PQ.OrdPSQ WorldSimpleCoords2D Turn () }
  deriving ( Eq, Show, Typeable, Generic )

addTurn :: GameState -> GameState
addTurn gs = removeOldSnowSteps $ gs { turn = turn gs + 1 }

addSnowStep :: WorldSimpleCoords2D -> GameState -> GameState
addSnowStep coords gs =
  gs { snowSteps = PQ.insert coords (turn gs + 10) () (snowSteps gs) }

removeOldSnowSteps :: GameState -> GameState
removeOldSnowSteps gs = case PQ.findMin (snowSteps gs) of
  Just (_, expire_turn, _) | expire_turn < turn gs ->
    removeOldSnowSteps $ gs { snowSteps = PQ.deleteMin (snowSteps gs) }
  _ -> gs

hasSnowStep :: WorldSimpleCoords2D -> GameState -> Bool
hasSnowStep coords gs = isJust $ PQ.lookup coords (snowSteps gs)

{-# INLINE playerPositionL #-}
playerPositionL :: Lens' GameState WorldCoords2D
playerPositionL = lens get_it set_it
 where
  get_it gs = playerPosition gs
  set_it gs pos = gs { playerPosition = pos }

{-# INLINE totalEnergyModulesL #-}
totalEnergyModulesL :: Lens' GameState Int
totalEnergyModulesL = lens get_it set_it
 where
  get_it gs = totalEnergyModules gs
  set_it gs !tem = gs { totalEnergyModules = tem }

{-# INLINE collectedEnergyModulesL #-}
collectedEnergyModulesL :: Lens' GameState Int
collectedEnergyModulesL = lens get_it set_it
 where
  get_it gs = collectedEnergyModules gs
  set_it gs count = gs { collectedEnergyModules = count }

{-# INLINE usedEnergyModulesL #-}
usedEnergyModulesL :: Lens' GameState Int
usedEnergyModulesL = lens get_it set_it
 where
  get_it gs = usedEnergyModules gs
  set_it gs count = gs { usedEnergyModules = count }

{-# INLINE collectedEnergyModuleLevelsL #-}
collectedEnergyModuleLevelsL :: Int -> Lens' GameState Bool
collectedEnergyModuleLevelsL lkey = lens get_it set_it
 where
  get_it gs = lkey `IS.member` collectedEnergyModuleLevels gs
  set_it gs True = gs
    { collectedEnergyModuleLevels = IS.insert lkey
                                              (collectedEnergyModuleLevels gs)
    }
  set_it gs False = gs
    { collectedEnergyModuleLevels = IS.delete lkey
                                              (collectedEnergyModuleLevels gs)
    }

data NPC = NPC
  { follows :: !Bool }
  deriving ( Eq, Ord, Show, Typeable, Data, Generic )

data Chomp = Chomp !(Chain WorldSimpleCoords2D) !Int
  deriving ( Eq, Ord, Show, Typeable, Data, Generic )

initialChomp :: WorldSimpleCoords2D -> Int -> Chomp
initialChomp = Chomp . singlePartChain

{-# INLINE laserAtGS #-}
laserAtGS
  :: LiftLevel coords WorldSimpleCoords2D
  => coords
  -> Lens' GameState (Maybe (LaserColor, TileOrientation))
laserAtGS (liftLevel -> coords@(WorldSimpleCoords2D level_key _)) = lens
  get_it
  set_it
 where
  get_it gs = gs ^. lasersL . laserAt (gs ^. worldL) coords
  set_it gs laser = updateReceptablesOnLevel
    level_key
    (gs & (lasersL . laserAt (gs ^. worldL) coords .~ laser))

updateReceptablesOnLevel :: Int -> GameState -> GameState
updateReceptablesOnLevel level_key gs = foldl'
  folder
  gs
  (fromMaybe S.empty $ gs ^. laserReceptablesL . at level_key)
 where
  folder gs receptable_coords =
    updateReceptable (WorldSimpleCoords2D level_key receptable_coords) gs

updateLasersAtGS :: WorldSimpleCoords2D -> GameState -> GameState
updateLasersAtGS coords gs =
  gs & lasersL %~ updateLaserAt (gs ^. worldL) coords

{-# INLINE lasersL #-}
lasersL :: Lens' GameState LaserState
lasersL = lens get_it set_it
 where
  get_it gs = lasers gs
  set_it gs d = gs { lasers = d }

{-# INLINE laserReceptablesL #-}
laserReceptablesL :: Lens' GameState (M.Map Int (S.Set Coords2D))
laserReceptablesL = lens get_it set_it
 where
  get_it gs = laserReceptables gs
  set_it gs d = gs { laserReceptables = d }

updateReceptable :: WorldSimpleCoords2D -> GameState -> GameState
updateReceptable wcoords@(WorldSimpleCoords2D level_key coords) gs =
  case
      ( gs ^. laserReceptablesL . at level_key
      , tileAt (gs ^. worldL) (toWorldCoords wcoords)
      )
    of
      (Just receptable, tile)
        | coords `S.member` receptable, receptable_color <- receptableColor tile
        -> case laserStatusAt (gs ^. lasersL) wcoords of
          LaserAt lasercolor _ Strong | lasercolor == receptable_color ->
            updateColoredDoor wcoords lasercolor True gs
          _ -> updateColoredDoor wcoords receptable_color False gs
      _ -> gs

{-# INLINE toWorldCoords #-}
toWorldCoords :: WorldSimpleCoords2D -> WorldCoords2D
toWorldCoords (WorldSimpleCoords2D level_key coords) =
  WorldCoords2D level_key (SwizzCoords2D coords Rot0 NoSwizzle NoSwizzle)

updateColoredDoor
  :: WorldSimpleCoords2D -> LaserColor -> Bool -> GameState -> GameState
updateColoredDoor wcoords lasercolor toggle gs =
  case
      lookForTile (toWorldCoords wcoords) (gs ^. worldL) 1000
        $ \_ block -> doorColor block == Just lasercolor
    of
      Nothing            -> gs
      Just (pos, _block) -> go gs pos S.empty
 where
  go gs pos visited' =
    let !gs' =
          updateLasersAtGS (toSimpleCoords pos)
            $  gs
            &  worldL
            .  tileAtL pos
            .~ (coloredDoor lasercolor toggle)
        !visited = S.insert pos visited'
    in  case
          lookForTile pos (gs' ^. worldL) 20 $ \coords block ->
            coords
              `S.notMember` visited
              &&            coords
              /=            pos
              &&            doorColor block
              ==            Just lasercolor
        of
          Nothing             -> gs'
          Just (pos', _block) -> go gs' pos' visited

{-# INLINE respawnPointsL #-}
respawnPointsL :: Lens' GameState (M.Map Int Coords2D)
respawnPointsL = lens get_it set_it
 where
  get_it gs = respawnPoints gs
  set_it gs d = gs { respawnPoints = d }

{-# INLINE deadL #-}
deadL :: Lens' GameState Bool
deadL = lens get_it set_it
 where
  get_it gs = dead gs
  set_it gs d = gs { dead = d }

{-# INLINE chompChainsL #-}
chompChainsL :: Lens' GameState (M.Map WorldSimpleCoords2D Int)
chompChainsL = lens get_it set_it
 where
  get_it = chompChains
  set_it old chomps = old { chompChains = chomps }

{-# INLINE chompiesL #-}
chompiesL :: Lens' GameState (M.Map WorldSimpleCoords2D Chomp)
chompiesL = lens get_it set_it
 where
  get_it = chompies
  set_it old chomps = old { chompies = chomps }

{-# INLINE tileAtL #-}
tileAtL
  :: (Eq tile, Default tile, LiftLevel r WorldSimpleCoords2D)
  => r
  -> Lens' (TileWorld tile) tile
tileAtL wcoords = lens get_it set_it
 where
  wcoords' =
    let WorldSimpleCoords2D level_key coords = liftLevel wcoords
    in  WorldCoords2D level_key (SwizzCoords2D coords Rot0 NoSwizzle NoSwizzle)

  get_it world = tileAt world wcoords'
  set_it world tile = setTile world wcoords' tile

{-# INLINE worldL #-}
worldL :: Lens' GameState (TileWorld Tile)
worldL = lens get_it set_it
 where
  get_it = world
  set_it old world = old { world = world }

newtype GSM a = GSM (StateT GameState (Except ()) a)
  deriving ( Functor, Applicative, Monad, MonadState GameState, Alternative, MonadPlus )

{-# INLINE runGameState #-}
runGameState :: GSM a -> GameState -> Maybe (GameState, a)
runGameState (GSM st) game_state = case runExcept (runStateT st game_state) of
  Left  ()           -> Nothing
  Right (result, st) -> Just (st, result)
