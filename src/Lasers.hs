{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}

module Lasers
  ( LaserState()
  , LaserStatusAt(..)
  , LaserColor(..)
  , Strength(..)
  , laserAt
  , emptyLaserState
  , setLaser
  , removeLaser
  , laserStatusAt
  , updateLaserAt
  )
where


import           Control.Lens            hiding ( Context )
import           Control.Monad.State.Strict
import           Data.Data
import           Data.Foldable
import qualified Data.Map.Strict               as M
import           Data.Maybe
import qualified Data.Set                      as S
import           GHC.Generics

import           CM.Coords
import           CM.LevelLike
import           CM.LiftLevel
import           CM.Portal
import           CM.TileWorld

import           Tiles

type LaserStrengths = M.Map WorldSimpleCoords2D Strength
type LaserPaths = M.Map WorldSimpleCoords2D (M.Map WorldSimpleCoords2D TileOrientation)
type LaserSources = M.Map WorldSimpleCoords2D (LaserColor, TileOrientation, [WorldSimpleCoords2D])

data LaserState = LaserState {
    _laserPaths :: !LaserPaths
  , _laserSources :: !LaserSources
  , _laserStrengths :: !LaserStrengths
  }
  deriving ( Eq, Ord, Show, Typeable, Data, Generic )

data LaserRecomp = LaserRecomp {
    _innerLaserState :: !LaserState
  , _recheckBeams :: !(S.Set WorldSimpleCoords2D)
  }
  deriving ( Eq, Ord, Show, Typeable, Data, Generic )

data LaserStatusAt
  = NoLaser
  | LaserAt !LaserColor !TileOrientation !Strength
  | LaserCollision
  deriving ( Eq, Ord, Show, Typeable, Data, Generic )

{-# INLINE laserStatusAt #-}
laserStatusAt :: LaserState -> WorldSimpleCoords2D -> LaserStatusAt
laserStatusAt ls coords =
  case (ls ^. laserPaths . at coords, ls ^. laserStrengths . at coords) of
    (Just sources, _) | M.size sources > 1 -> LaserCollision
    (Just sources, Just strength) | M.size sources == 1 ->
      let (source, rot) = M.findMin sources
      in  case ls ^. laserSources . at source of
            Nothing            -> error "laserStatusAt: impossible! no source"
            Just (color, _, _) -> LaserAt color rot strength
    (Nothing, Nothing) -> NoLaser
    _                  -> error "laserStatusAt: impossible! mismatch"

{-# INLINE recheckBeams #-}
recheckBeams :: Lens' LaserRecomp (S.Set WorldSimpleCoords2D)
recheckBeams = lens get_it set_it
 where
  get_it = _recheckBeams
  set_it old new = old { _recheckBeams = new }

class HasLaserState a where
  laserState :: Lens' a LaserState

instance HasLaserState LaserState where
  {-# INLINE laserState #-}
  laserState = lens id (\_ ni -> ni)

instance HasLaserState LaserRecomp where
  {-# INLINE laserState #-}
  laserState = lens get_it set_it
   where
    get_it laser_recomp = _innerLaserState laser_recomp
    set_it old new = old { _innerLaserState = new }

{-# INLINE laserStrengths #-}
laserStrengths :: HasLaserState a => Lens' a LaserStrengths
laserStrengths = lens get_it set_it
 where
  get_it ls = _laserStrengths $ ls ^. laserState
  set_it ls new = ls & laserState %~ \old -> old { _laserStrengths = new }

{-# INLINE laserPaths #-}
laserPaths :: HasLaserState a => Lens' a LaserPaths
laserPaths = lens get_it set_it
 where
  get_it ls = _laserPaths $ ls ^. laserState
  set_it ls new = ls & laserState %~ \old -> old { _laserPaths = new }

{-# INLINE laserSources #-}
laserSources :: HasLaserState a => Lens' a LaserSources
laserSources = lens get_it set_it
 where
  get_it ls = _laserSources $ ls ^. laserState
  set_it ls new = ls & laserState %~ \old -> old { _laserSources = new }

emptyLaserState :: LaserState
emptyLaserState = LaserState M.empty M.empty M.empty

oriToMove
  :: (TiledCoordMoving coords, Context coords ~ w', LiftLevel w w')
  => coords
  -> w
  -> TileOrientation
  -> coords
oriToMove coords tworld OnTop    = toUp (liftLevel tworld) coords
oriToMove coords tworld OnBottom = toDown (liftLevel tworld) coords
oriToMove coords tworld OnLeft   = toLeft (liftLevel tworld) coords
oriToMove coords tworld OnRight  = toRight (liftLevel tworld) coords

{-# INLINE toWorldCoords #-}
toWorldCoords :: WorldSimpleCoords2D -> WorldCoords2D
toWorldCoords (WorldSimpleCoords2D level_key coords) =
  WorldCoords2D level_key (SwizzCoords2D coords Rot0 NoSwizzle NoSwizzle)

removeLaser :: WorldSimpleCoords2D -> TileWorld Tile -> LaserState -> LaserState
removeLaser coords tworld lstate =
  _innerLaserState
    $ flip execState (LaserRecomp lstate S.empty)
    $ do
        old_laser_src <- use $ laserSources . at coords
        -- Check if there's actually a laser at the specified coordinates.
        -- If there isn't one, then we do nothing.
        case old_laser_src of
          Nothing                  -> return ()
          Just (_, rot, positions) -> do
            removeLaser' rot positions
            recheckLaserStrengths tworld
 where
  removeLaser' _rot positions = do
    laserSources . at coords .= Nothing
    for_ positions $ \(!coord) -> do
      !existing_paths <- fmap (fromMaybe M.empty) $ use $ laserPaths . at coord
      let new_paths = M.delete coords existing_paths
      if M.null new_paths
        then do
          laserPaths . at coord .= Nothing
          laserStrengths . at coord .= Nothing
        else do
          laserPaths . at coord .= Just new_paths
          for_ (M.keys new_paths)
            $ \src_coords -> recheckBeams %= S.insert src_coords

{-# INLINE withBeam #-}
withBeam
  :: Applicative f
  => TileWorld Tile
  -> WorldSimpleCoords2D
  -> TileOrientation
  -> (WorldSimpleCoords2D -> TileOrientation -> f ())
  -> f ()
withBeam tworld (toWorldCoords -> start_coord) rot action = go start_coord
                                                               rot
                                                               (200 :: Int)
 where
  go _      _    0      = pure ()
  go !coord !rot !count = do
    let !tile = tileAt tworld coord
    if
      | isLaserPassable tile -> action (toSimpleCoords coord) rot
      *> go (oriToMove coord tworld rot) rot (count - 1)
      | InactiveLaser ori <- tile -> action (toSimpleCoords coord) rot
      *> go (oriToMove coord tworld ori) ori (count - 1)
      | otherwise -> action (toSimpleCoords coord) rot

{-# INLINE laserAt #-}
laserAt
  :: TileWorld Tile
  -> WorldSimpleCoords2D
  -> Lens' LaserState (Maybe (LaserColor, TileOrientation))
laserAt tworld coords = lens get_it set_it
 where
  get_it lstate = case lstate ^. laserSources . at coords of
    Nothing               -> Nothing
    Just (lcolor, ori, _) -> Just (lcolor, ori)
  set_it lstate Nothing = removeLaser coords tworld lstate
  set_it lstate (Just (color, ori)) =
    setLaser coords tworld color ori $ removeLaser coords tworld lstate

setLaser
  :: WorldSimpleCoords2D
  -> TileWorld Tile
  -> LaserColor
  -> TileOrientation
  -> LaserState
  -> LaserState
setLaser coords tworld lcolor rot lstate =
  _innerLaserState
    $ flip execState (LaserRecomp lstate S.empty)
    $ do
  -- Check invariant: no laser source already at the location
        old_laser_src <- use $ laserSources . at coords
        when (isJust old_laser_src)
          $ error
              "setLaser: attempted to set laser to something that already had laser."

        laserSources . at coords .= Just (lcolor, rot, [])

        let !start_coord = oriToMove coords tworld rot
        recheckBeams .= S.singleton coords

        beam_positions <- layDownLaserBeam start_coord
        laserSources . at coords %= \(Just (lcolor, rot, _)) ->
          Just (lcolor, rot, reverse beam_positions)
        recheckLaserStrengths tworld
 where
  layDownLaserBeam start_coord =
    flip execStateT []
      $ withBeam tworld start_coord rot
      $ \(!coord) (!rot) -> do
          !existing_paths <-
            fmap (fromMaybe M.empty) $ lift $ use $ laserPaths . at coord
          lift $ when (not $ M.null existing_paths) $ do
            for_ (M.keys existing_paths) $ \existing_path_src ->
              recheckBeams %= S.insert existing_path_src
          lift $ laserPaths . at coord %= Just . \case
            Nothing  -> M.singleton coords rot
            Just set -> M.insert coords rot set
          modify $ (:) coord

updateLaserAt
  :: TileWorld Tile -> WorldSimpleCoords2D -> LaserState -> LaserState
updateLaserAt tworld coords ls = flip execState ls $ do
  sourceCheck coords
  pathCheck
 where
  pathCheck = do
    ls <- get
    case ls ^. laserPaths . at coords of
      Just sources -> for_ (M.keys sources) sourceCheck
      _            -> return ()

  sourceCheck coords = do
    ls <- get
    case ls ^. laserSources . at coords of
      Just (lcolor, ori, _) ->
        put $ setLaser coords tworld lcolor ori $ removeLaser coords tworld ls
      _ -> return ()

recheckLaserStrengths :: MonadState LaserRecomp m => TileWorld Tile -> m ()
recheckLaserStrengths _tworld = do
  recheckables <- use recheckBeams
  for_ recheckables $ \src_coords -> do
    sources <- use laserSources
    let (_color, _rot, positions) =
          fromMaybe (error "Inconsistent laser state; no source found")
            $ M.lookup src_coords sources
    flip execStateT Strong $ for_ positions $ \(!coord) -> do
      paths <- fmap (fromMaybe M.empty) $ lift $ use $ laserPaths . at coord
      let paths' = M.delete src_coords paths
      unless (M.null paths') $ put Weak
      !strength <- get
      lift $ laserStrengths . at coord .= Just strength
