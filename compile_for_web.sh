#!/usr/bin/env bash

set -euox pipefail

stack build --stack-yaml stack-browser.yaml
cd web
closure -O ADVANCED \
    --externs ../.stack-work/install/x86_64-linux/lts-9.21/ghcjs-0.2.1.9009021_ghc-8.0.2/bin/baconist.jsexe/all.js.externs \
    --jscomp_off checkVars bundle.js > bundle.min.js
